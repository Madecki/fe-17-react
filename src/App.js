import React from 'react';
import './App.css';
import LandingPage from './Components/LandingPage/LandingPage';
import Nav from './Components/Nav/Nav';

function App() {
  return (
    <>
      <Nav></Nav>
      <main>
        <LandingPage></LandingPage>
      </main>
    </>
  );
}

export default App;
