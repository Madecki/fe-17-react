import React from 'react';
import './Nav.css';

function Nav() {
  return (
    <nav>
      <div className="container container-flex">
        <a href className="nav-logo">moja firma</a>

        <div className="hamburger-menu">
            <input className="menu-btn" type="checkbox" id="menu-btn" />
            <label className="menu-icon" for="menu-btn"><span className="navicon"></span></label>
            <ul className="dropdown-content">
            <li><a href="#about-us">o nas</a></li><br />
            <li><a href="#offer">oferta</a></li><br />
            <li><a href="#offer">oferta</a></li><br />
            <li><a href className="not-allowed">kontakt</a></li>
            </ul>
        </div>
        <div className="nav-links">
            <a href="#about">o nas</a>
            <a href="#offer">oferta</a>
            <a href className="not-allowed">kontakt</a>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
