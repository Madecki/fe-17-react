import React from 'react';
import './LandingPage.css';

function LandingPage() {
  return (
    <section className="landing-page">
      <div className="container container-flex-column">
          <div className="landing-page-box-text container-flex-column">
              <h1>Nasza firma oferuje najwyższej jakości produkty</h1>
              <h2 className="text-white">Nie wierz nam na słowo - sprawdź!</h2>
          </div>
          <a  className="button-landing-page" href="#offer">Oferta</a>
      </div>
    </section>
  );
}

export default LandingPage;
